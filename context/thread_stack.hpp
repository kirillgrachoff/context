#pragma once

#include <wheels/memory/view.hpp>

namespace context {

wheels::MutableMemView ThisThreadStack();

}  // namespace context
